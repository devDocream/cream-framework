module.exports = function (grunt) {
    grunt.initConfig( {
        stealBuild : {
            demosb : {
                options : {
                    system       : {
                        main   : "src/DoCream/DemoBunch/Sandbox/DemoSandbox",
                        config : "cream.config.js"
                    },
                    buildOptions : {
                        bundleSteal : false,
                        minify      : false,
                        bundleDepth : 10,
                        mainDepth   : 10
                    }
                }
            },
            app    : {
                options : {
                    system       : {
                        main   : "app",
                        config : "cream.build.js"
                    },
                    buildOptions : {
                        bundleSteal : false,
                        minify      : true
                    }

                }
            }

        },
        jsdoc : {
            dist : {
                src: ['src/**/*.js'],
                options: {
                    destination: 'dist/doc',
                    configure: 'jsdoc.config.json',
                    template: 'node_modules/ink-docstrap/template',
                    private: true
                }
            }
        },
        connect: {
            test : {
                port : 8001
            }
        },
        jasmine: {
            coverage: {
                src: 'src/**/*.js',
                options: {
                    specs: 'src/**/*Spec.js',
                    helpers: 'src/**/*Helper.js',
                    host: 'http://127.0.0.1:8000/',
                    template: require('grunt-template-jasmine-istanbul') ,
                    templateOptions: {
                        coverage: 'dist/reports/coverage.json',
                        report: {
                            type: 'cobertura',
                            options: {
                                dir: 'dist/reports'
                            }
                        },
                        template: require('grunt-creamjs-jasmine'),
                        templateOptions: {
                            requireConfig: {
                                baseUrl: '.grunt/grunt-contrib-jasmine/src/main/js/'
                            }
                        }
                    },
                    junit: {
                        path: 'dist/reports',
                        consolidate: true
                    }
                }
            },
            coverage2: {
                src: 'vendor/creamjs/src/**/*.js',
                options: {
                    specs: 'vendor/creamjs/spec/**/*Spec.js',
                    helpers: 'src/**/*Helper.js',
                    host: 'http://127.0.0.1:8000/',
                    template: require('grunt-template-jasmine-istanbul') ,
                    templateOptions: {
                        coverage: 'dist/reports/coverage.json',
                        report: {
                            type: 'cobertura',
                            options: {
                                dir: 'dist/reports'
                            }
                        },
                        template: require('grunt-creamjs-jasmine'),
                        templateOptions: {
                            requireConfig: {
                                baseUrl: '.grunt/grunt-contrib-jasmine/src/main/js/'
                            }
                        }
                    },
                    junit: {
                        path: 'dist/reports',
                        consolidate: true
                    }
                }
            }
        },
        eslint: {
            js:{
                src: ['src/**/*.js'],
                options: {
                    config: '.eslintrc',
                    format: 'checkstyle',
                    outputFile: 'dist/reports/checkstyle-eslint.xml'
                },
                force: true
            }

        },
        jscs:{
            js: {
                src: "src/**/*.js",
                options: {
                    config: ".jscsrc",
                    force: true,
                    reporter: 'checkstyle',
                    reporterOutput: 'dist/reports/checkstyle-jscs.xml'
                }
            }
        },
        jscpd: {
            javascript: {
                output: 'dist/reports/js.cpd.xml',
                path: 'src',
                exclude: ['vendor/**', 'apps/**','.grunt','node_modules']
            }
        },
        csslint: {
            src: ['dist/bundles/**/*.css'],
            options:{
                formatters: [
                    {id: 'checkstyle-xml', dest: 'dist/reports/checkstyle-css.xml'}
                ]
            }
        },
        cssmin:{
            cssmin: {
                expand: true,
                cwd: 'dist/bundles',
                src: ['**/*.css', '!*.min.css'],
                dest: 'dist/bundles'
            }
        },
        uglify: {
            options: {
                compress: {
                    drop_console: true
                }
            },
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'dist/bundles',
                    src: ['**/*.js','!app.js'],
                    dest: 'dist/bundles'
                }]
            }
        },
        imagemin: {                          // Task
            imgmin: {                         // Another target
                files: [{
                    expand: true,
                    cwd: 'src/',// Enable dynamic expansion
                    src: ['**/*.{png,jpg,gif,svg,jpeg}'],   // Actual patterns to match
                    dest: 'dist/bundles/img/'                  // Destination path prefix
                }]
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'gzip'
                },
                expand: true,
                cwd: 'dist/bundles',
                src: ['**/*.js'],
                dest: 'public/'
            }
        },
       replace: {
           another_example: {
               src: ['dist/bundles/*.css'],
               overwrite: true,                 // overwrite matched source files
               replacements: [{
                   from: /(\.{2})\/((\.{2})\/src\/)/g,
                   to: function () {
                       return "img/";
                  }
               }]
           }
       }


    });

    grunt.loadTasks('node_modules/steal-tools/tasks');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks("grunt-jscs");
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-jscpd');
    grunt.loadNpmTasks('grunt-contrib-csslint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-compress');

    grunt.registerTask('build',['stealBuild']);
    grunt.registerTask('doc',['jsdoc']);
    grunt.registerTask('test',['connect','jasmine:coverage']);
    grunt.registerTask('test2',['connect','jasmine:coverage2']);
    grunt.registerTask('checkjs',['eslint', 'jscs']);

    grunt.registerTask('cpd',['jscpd']);
    grunt.registerTask('checkcss',['csslint']);
    grunt.registerTask('min',['uglify','cssmin']);
    grunt.registerTask('compress',['compress']);
    grunt.registerTask('imgmin',['imagemin']);
    grunt.registerTask('cssreplace',['replace']);
    grunt.registerTask('flow', ['test', 'checkjs', 'cpd','doc', 'build','checkcss','min','cssreplace','imgmin']);
};