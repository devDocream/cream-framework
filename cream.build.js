(function () {
    System.import('apps/CreamRoutes');
    System.config({
        paths: {
            'creamjs/*'     : 'vendor/creamjs/*.js',
            'cream/*'       : 'vendor/creamjs/src/*.js',
            'component/*'   : 'vendor/creamjs/src/Component/*.js',
            'framework/*'   : 'vendor/creamjs/src/Framework/*.js',
            'model/*'       : 'vendor/creamjs/src/Component/Model/*.js',
            'service/*'     : 'vendor/creamjs/src/Service/*.js',
            'interface/*'   : 'vendor/creamjs/src/Component/Interface/*.js',
            'kernel/*'      : 'vendor/creamjs/src/Framework/Kernel/*.js',
            'adapter/*'     : 'vendor/creamjs/src/Adapter/*.js',
            'jquery/*'      : 'vendor/jquery/dist/*.js',
            'Util/*'        : 'vendor/creamjs/src/Framework/Util/*.js',
            'twig/*'        : 'vendor/twig.js/*.js',
            'bunch/*'       : 'vendor/creamjs/src/Framework/Bunch/*.js',
            '@traceur'      : 'vendor/traceur/traceur.js',
            '@config'       : 'vendor/creamjs/cream.config.js'
        },
        ext: {
            less: "vendor/steal/less",
            css: "vendor/steal/css",
            twig: "vendor/creamjs/src/Extension/steal/twig",
            service: "vendor/creamjs/src/Extension/steal/service",
            library: "vendor/creamjs/src/Extension/steal/library",
            engine: "vendor/creamjs/src/Extension/steal/engine",
            bunch:  "vendor/creamjs/src/Extension/steal/bunch",
            deps:  "vendor/creamjs/src/Extension/steal/deps"
        },

        defaults:{
            services:{
                View      : {
                    engine: 'twig',
                    helpers: {
                        'path': 'router.generate',
                        'asset': 'asset.asset',
                        'trans': 'translator.trans',
                        'transChoice': 'translator.transChoice'
                    }
                },
                Router: {
                    property: 'routes'
                },
                Translator:{
                    locale: 'es',
                    property: 'translations'
                },
                Asset: true,
                Ajax: true
                //,Types: true
            },
            lib: 'jquery'
        }

    });

})();