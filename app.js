define(['framework/Routing/Routing'], function(Routing){


    var route =  Routing.match();

    if ( System.env == "production")
    {
        window.define = System.define;
        System.import(route.value.production).then( function( scr  ){

            System.import('src/' + route.value.development).then(function(sb){
            });
        });
    }
    else{
        System.import(route.value.development).then(function(sb){

        });

    }


    return route;
});