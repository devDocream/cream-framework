define(
function DemoBunch ()
{
    return  {

        Cream   :
        {
            services : {
                View      : {
                    helpers: {
                        'capsTu' : 'str.capitalize'
                    }
                },
                 Translator: false,
                Validator:
                {
                    asserts:
                    {
                        spanishDate1: {
                            fn: 'Str.isSpanishDate',
                            message: 'The value is not valid spanish date '
                        }
                    },

                    types:
                    {
                        spanishDate1: {
                            fn: 'Str.isSpanishDate',
                            message: 'The value is not valid spanish date '
                        }
                    }
                }
            },
            sandbox:{
                data: {
                }
            },

            utils :
            {

            }
        },
        Demo    :
        {
            services    :
            {
                View      : {
                    helpers: {
                        'capsTu' : 'str.capitalize2'
                    }
                },
                String : {
                    path : 'DoCream/DemoBunch/Service/StringService'
                }
            },

            sandbox     :
            {
                    data :
                    {
                        hola : 'desdeSandbox'
                    }
            },

            utils       :
            {

            }
        }
    };

} );
