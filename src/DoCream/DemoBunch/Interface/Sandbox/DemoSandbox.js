define( [
        'cream'
],
function demoSandbox ( cream )
{
    return cream.sandbox(
        'DoCream.Demo.Sandbox.Demo',
        {
            attr : 'attr'
        },
        {
            pro : 'private'
        },
        {
            attr : 'public'
        },
        {
            attr : 'events'
        },
        {
            attr : 'services'
        },
        {
            attr : 'collections'
        }
    );
} );
