define(
function DoCream ()
{
    return  {

        Cream: {
            services:{
                View      : {
                    helpers: {
                        'capsTu' : 'str.capitalize'
                    }
                }/*,,
                 Translator: false
                Validator:
                {
                    asserts:
                    {
                        spanishDate1: {
                            fn: 'Str.isSpanishDate',
                            message: 'The value is not valid spanish date '
                        }
                    },

                    types:
                    {
                        spanishDate1: {
                            fn: 'Str.isSpanishDate',
                            message: 'The value is not valid spanish date '
                        }
                    }
                }*/
            },

            sandbox:{
                data: {
                    'users'         : ['Repository:User.users', 'Service:Str.init', 'Collection:/ELEMENTS/', 'widget:label:label0.title'],
                    'workStations'  : ['Kit:left.stations']
                }
            },
            utils: {
                        arr : 'DoCream/DemoBunch/Util/Array'
            }
        }
    };


    /**
     * @namespace DoCream.DemoBunch
     * @parent DoCream
     * @description Demo bunch container
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */


    /**
     * @namespace DoCream.DemoBunch.Interface
     * @parent DoCream.DemoBunch
     * @description Demo bunch interface
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */

    /**
     * @namespace DoCream.DemoBunch.Interface.Kit
     * @parent DoCream.DemoBunch.Interface
     * @description Demo bunch kits
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */

    /**
     * @namespace DoCream.DemoBunch.Interface.Plugin
     * @parent DoCream.DemoBunch.Interface
     * @description Demo bunch kits
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */

    /**
     * @namespace DoCream.DemoBunch.Interface.Widget
     * @parent DoCream.DemoBunch.Interface
     * @description Demo bunch kits
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */

    /**
     * @namespace DoCream.DemoBunch.Sandbox
     * @parent DoCream.DemoBunch
     * @description Demo bunch sandboxes
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */

} );
