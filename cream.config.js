if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function () {},
            fBound = function () {
                return fToBind.apply(this instanceof fNOP && oThis
                    ? this
                    : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}

require( 'apps/CreamRoutes' );
require( 'apps/CreamConfig' );
require( 'apps/CreamSpaces' );
( function creamconfig ()
{

    System.import( 'apps/CreamRoutes' );
    System.import( 'apps/CreamConfig' );
    System.import( 'apps/CreamSpaces' );

    /**
     * @namespace Cream
     * @description Cream classes container
     * @global
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */
    Cream = {};

    /**
     * @namespace cream
     * @description Cream  library container
     * @global
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */
    cream = {};

    System.config( {
        Cream: {
            templates : []
        },
        paths: {
            'creamjs/*'     : 'vendor/creamjs/*.js',
            'cream'         : 'vendor/creamjs/src/cream.js',
            'cream/*'       : 'vendor/creamjs/src/*.js',
            'configure'     : 'vendor/creamjs/src/Framework/Bootstrap/Configure.js',
            'component/*'   : 'vendor/creamjs/src/Component/*.js',
            'class'         : 'vendor/creamjs/src/Component/Class/Class.js',
            'boot'          : 'vendor/creamjs/src/Framework/Bootstrap/Bootstrap.js',
            'framework/*'   : 'vendor/creamjs/src/Framework/*.js',
            'framework'     : 'vendor/creamjs/src/Framework/Framework.js',
            'model/*'       : 'vendor/creamjs/src/Component/Model/*.js',
            'service/*'     : 'vendor/creamjs/src/Service/*.js',
            'service'       : 'vendor/creamjs/src/Component/Service/Service.js',
            'widget'        : 'vendor/creamjs/src/Component/Interface/Widget/Widget.js',
            'plugin'        : 'vendor/creamjs/src/Component/Interface/Plugin/Plugin.js',
            'sandbox'       : 'vendor/creamjs/src/Component/Interface/Sandbox/Sandbox.js',
            'repository'    : 'vendor/creamjs/src/Component/Model/Repository/Repository.js',
            'kit'           : 'vendor/creamjs/src/Component/Interface/Kit/Kit.js',
            'interface/*'   : 'vendor/creamjs/src/Component/Interface/*.js',
            'kernel/*'      : 'vendor/creamjs/src/Framework/Kernel/*.js',
            'adapter/*'     : 'vendor/creamjs/src/Adapter/*.js',
            'jquery'        : 'vendor/jquery/dist/jquery.min.js',
            'library'       : 'vendor/creamjs/src/Adapter/Library/lib.library',
            'util/*'        : 'vendor/creamjs/src/Util/*.js',
            'twig'          : 'vendor/twig.js/twig.min.js',
            'bunch/*'       : 'vendor/creamjs/src/Framework/Bunch/*.js',
            '@traceur'      : 'vendor/traceur/traceur.js',
            '@config'       : 'vendor/creamjs/cream.config.js',
            'src/*'         : 'src/*.js'
        },
        ext: {
            less: "vendor/steal/less",
            css: "vendor/steal/css",
            twig: "vendor/creamjs/src/Extension/twig",
            service: "vendor/creamjs/src/Extension/services",
            library: "vendor/creamjs/src/Extension/library",
            engine: "vendor/creamjs/src/Extension/viewengine",
            bunch:  "vendor/creamjs/src/Extension/bunch",
            deps:  "vendor/creamjs/src/Extension/steal/deps",
            util:  "vendor/creamjs/src/Extension/utils",
            boot: "vendor/creamjs/src/Extension/steal/boot"
        },
        meta: {
            "jquery": {
                format: "global"
            }
        },
        defaults:{
            libraries : ['jQuery'],
            services:{
                View      : {
                    engine  : 'twig',
                    filters :
                    {
                        'trans': 'translator.trans',
                        'transChoice': 'translator.transChoice'
                    },
                    helpers:
                    {
                        'path': 'router.generate',
                        'asset': 'asset.asset'
                    }
                },
                Interface: true,
                Event    : true,
                Ajax     : true,
                Asset: {
                    base    : 'auto'
                },
                Router: {
                            property    : 'routes'
                },
                Translator:{
                            locales     : ['es'],
                            property    : 'translations',
                            domains     : ['messages']
                            //domains: ['messages']
                }/*,
                ,
                Ajax: true,
                URL: true,
                Session: true,
                Cache: true,
                Model: true,
                Validator: {
                        asserts:{
                        }
                }*/

                //,Types: true
            },
            lib     : 'jQuery',
            utils   : {
                        asserts : 'Assert',
                        object  : 'Object',
                        string  : 'String',
                        date    : 'Date'
            }
        }

    } );

} )();
